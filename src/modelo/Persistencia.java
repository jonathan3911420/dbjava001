/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo;
import java.util.ArrayList;

/**
 *
 * @author efrai
 */
public interface Persistencia {
    public void insertar (Object object) throws Exception;
    public void actualizar (Object object) throws Exception;
    public void habilitar (Object object) throws Exception;
    public void desahabilitar (Object object) throws Exception;
    public void borrarRegistro (Object object) throws Exception;
    public boolean siExiste(int id) throws Exception;
    
    public ArrayList lista() throws Exception;
    public Object buscar(String codigo) throws Exception;
}

