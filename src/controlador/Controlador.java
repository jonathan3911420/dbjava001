/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador;

import modelo.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import javax.swing.JOptionPane;
import vista.jifProductos;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import javax.swing.table.DefaultTableModel;

public class Controlador implements ActionListener {
    private dbProducto db;
    private jifProductos vista;
    private boolean esActualizar;
    private int idproducto;

    // Constructor
    public Controlador(dbProducto db, jifProductos vista) {
        this.db = db;
        this.vista = vista;
        
        // Registrar los eventos de clic para los botones
        vista.btnBuscar.addActionListener(this);
        vista.btnCancelar.addActionListener(this);
        vista.btnCerrar.addActionListener(this);
        vista.btnDeshabilitar.addActionListener(this);
        vista.btnGuardar.addActionListener(this);
        vista.btnLimpiar.addActionListener(this);
        vista.btnNuevo.addActionListener(this);
    }

    // Método para inicializar la vista
    public void iniciarvista() {
        vista.setTitle(":: Productos ::");
        vista.setVisible(true);
        vista.setSize(1000, 600);  // Cambié resize por setSize
        this.deshabilitar();
    }

    // Método para convertir una fecha a un formato específico
    public String convertirAñoMesDia(Date fecha) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(fecha);
    }

    // Método para convertir una cadena a una fecha y establecerla en el componente de fecha
    public void convertirAñoMesDia(String fecha) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date date = dateFormat.parse(fecha);
            vista.jdcFecha.setDate(date);
        } catch (ParseException e) {
            System.err.print(e.getMessage());
        }
    }

    // Método para limpiar los campos de la vista
    public void limpiar() {
        vista.txtcodigo.setText("");
        vista.txtnombre.setText("");
        vista.txtprecio.setText("");
        vista.jdcFecha.setDate(new Date());
    }

    // Método para cerrar la aplicación
    public void cerrar() {
        int res = JOptionPane.showConfirmDialog(vista, "¿Desea cerrar el sistema?", "Productos", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        if (res == JOptionPane.YES_OPTION) {
            vista.dispose();
        }
    }

    // Método para habilitar los campos de la vista
    public void habilitar() {
        vista.txtcodigo.setEnabled(true);
        vista.txtnombre.setEnabled(true);
        vista.txtprecio.setEnabled(true);
        vista.btnBuscar.setEnabled(true);
        vista.btnGuardar.setEnabled(true);
    }

    // Método para deshabilitar los campos de la vista
    public void deshabilitar() {
        vista.txtcodigo.setEnabled(false);
        vista.txtnombre.setEnabled(false);
        vista.txtprecio.setEnabled(false);
        vista.btnBuscar.setEnabled(false);
        vista.btnDeshabilitar.setEnabled(false);
        vista.btnGuardar.setEnabled(false);
    }

    // Método para validar que los campos no estén vacíos
    public boolean validar() {
        return !(vista.txtcodigo.getText().isEmpty() || vista.txtnombre.getText().isEmpty() || vista.txtprecio.getText().isEmpty());
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        if (ae.getSource() == vista.btnLimpiar) {
            this.limpiar();
        } else if (ae.getSource() == vista.btnGuardar) {
            this.guardar();
        } else if (ae.getSource() == vista.btnCerrar) {
            this.cerrar();
        } else if (ae.getSource() == vista.btnNuevo) {
            this.nuevo();
        } else if (ae.getSource() == vista.btnBuscar) {
            this.buscar();
        } else if (ae.getSource() == vista.btnDeshabilitar) {
            this.deshabilitarProducto();
        }
    }

    // Método para manejar la acción de guardar
    public void guardar() {
        Productos pro = new Productos();
        if (this.validar()) {
            pro.setCodigo(vista.txtcodigo.getText());
            pro.setNombre(vista.txtnombre.getText());
            pro.setPrecio(Float.parseFloat(vista.txtprecio.getText()));
            pro.setFecha(this.convertirAñoMesDia(vista.jdcFecha.getDate()));
            try {
                if (!this.esActualizar) {
                    db.insertar(pro);
                    JOptionPane.showMessageDialog(vista, "Se agregó con éxito el producto con código " + vista.txtcodigo.getText());
                } else {
                    pro.setIdProductos(this.idproducto);
                    db.actualizar(pro);
                    JOptionPane.showMessageDialog(vista, "Se actualizó con éxito el producto con código " + vista.txtcodigo.getText());
                }
                this.limpiar();
                this.deshabilitar();
            } catch (Exception e) {
                JOptionPane.showMessageDialog(vista, "Surgió un error al guardar el producto: " + e.getMessage());
            }
        } else {
            JOptionPane.showMessageDialog(vista, "Falta información.");
        }
    }

    // Método para manejar la acción de nuevo
    public void nuevo() {
        this.limpiar();
        this.habilitar();
        this.esActualizar = false;
        vista.txtcodigo.requestFocus();
    }

    // Método para manejar la acción de buscar
    public void buscar() {
        Productos pro = new Productos();
        if (vista.txtcodigo.getText().isEmpty()) {
            JOptionPane.showMessageDialog(vista, "Favor de capturar el código.");
            vista.txtcodigo.requestFocus();
        } else {
            try {
                pro = (Productos) db.buscar(vista.txtcodigo.getText());
                if (pro.getIdProductos() != 0) {
                    vista.txtnombre.setText(pro.getNombre());
                    vista.txtprecio.setText(String.valueOf(pro.getPrecio()));
                    this.convertirAñoMesDia(pro.getFecha());
                    vista.btnGuardar.setEnabled(true);
                    vista.btnDeshabilitar.setEnabled(true);
                    this.esActualizar = true;
                    this.idproducto = pro.getIdProductos();
                } else {
                    JOptionPane.showMessageDialog(vista, "No se encontró el producto con código " + vista.txtcodigo.getText());
                }
            } catch (Exception e) {
                JOptionPane.showMessageDialog(vista, "Surgió un error al buscar: " + e.getMessage());
            }
        }
    }

    // Método para manejar la acción de deshabilitar producto
    public void deshabilitarProducto() {
        Productos pro = new Productos();
        pro.setIdProductos(this.idproducto);
        int res = JOptionPane.showConfirmDialog(vista, "¿Desea deshabilitar el producto?", "Productos", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        if (res == JOptionPane.YES_OPTION) {
            try {
                db.desahabilitar(pro);
                this.limpiar();
                this.deshabilitar();
            } catch (Exception e) {
                JOptionPane.showMessageDialog(vista, "Surgió un error al deshabilitar el producto: " + e.getMessage());
            }
        }
    }

    // Método para actualizar la tabla de productos
    public void actualizarTabla(ArrayList<Productos> arr) {
        String campos[] = {"idProducto", "Código", "Nombre", "Precio", "Fecha"};
        String[][] datos = new String[arr.size()][5];
        int renglón = 0;
        for (Productos registro : arr) {
            datos[renglón][0] = String.valueOf(registro.getIdProductos());
            datos[renglón][1] = registro.getCodigo();
            datos[renglón][2] = registro.getNombre();
            datos[renglón][3] = String.valueOf(registro.getPrecio());
            datos[renglón][4] = registro.getFecha();
            renglón++;
        }
        DefaultTableModel tb = new DefaultTableModel(datos, campos);
        vista.tblProductos.setModel(tb);
    }
}